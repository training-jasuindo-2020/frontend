package id.artivisi.training.jasuindo.frontend.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.Map;

@Component @Slf4j
public class BackendAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        log.debug("Username : {}", username);
        log.debug("Password : {}", password);

        ClientRegistration clientRegistration =
                clientRegistrationRepository.findByRegistrationId("backendapp");

        Map<String, Object> hasil = WebClient.builder()
                .defaultHeaders(header -> header.setBasicAuth(clientRegistration.getClientId(), clientRegistration.getClientSecret()))
                .build()
                .post().uri(clientRegistration.getProviderDetails().getTokenUri())
                .body(BodyInserters
                        .fromFormData("username", username)
                        .with("password", password)
                .with("grant_type", "password"))
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorMap(e -> new BadCredentialsException(e.getMessage()))
                .block();

        log.debug("access token : {}", hasil.get("access_token"));
        return new UsernamePasswordAuthenticationToken(username, password, new ArrayList<>());

        /* response
        gagal 400
        {
          "error": "invalid_grant",
          "error_description": "Bad credentials"
        }
        sukses 200
        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiYWRtaW4iLCJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTk0Nzg0OTUyLCJhdXRob3JpdGllcyI6WyJPUkdBTklaQVRJT05fVklFVyJdLCJqdGkiOiIzOTU5YzI2Ny0yOGNhLTQ0YTktOGExZS03YWNlOTExYTlmMjQiLCJjbGllbnRfaWQiOiJtb2JpbGVhcHAifQ.AyFe6EUWywCmQZEpRSOOvJoErkCSm1PskTc7hXC9psC9S6JIgEC05jnmpp6zfsQ7HoMZr8AQn2zKM0x8jyxIDounNz3rtDoKVCjodyb2yEg932s1lmD8O4cwyfupbaV61fmCHuZA2duyk6yJXzgYbZxe94FHEtprJwLFCPsgETL6juSyscP9nQD-Bzf3qZKrZt8lKxj7qXxQhl3Ql0tQIi-BJF1cQe1Tqau9zDhrSAH1kJhh43qifXrMT9YhIc1wOqOU3QZS7CdVoeRcluq8_mrg5oM3kQF7UDmop2CSPSaVx0BgieuUVCtN0P_Oqr6pfGALVcM-Gqp0EvrGDNJ2cw",
          "token_type": "bearer",
          "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiYWRtaW4iLCJyZWFkIiwid3JpdGUiXSwiYXRpIjoiMzk1OWMyNjctMjhjYS00NGE5LThhMWUtN2FjZTkxMWE5ZjI0IiwiZXhwIjoxNTk2ODU1NjUyLCJhdXRob3JpdGllcyI6WyJPUkdBTklaQVRJT05fVklFVyJdLCJqdGkiOiI0ZDU3ZDBhOC01ZTliLTQ2MTktOGY3OC1iYTljNWVlNzRiNjAiLCJjbGllbnRfaWQiOiJtb2JpbGVhcHAifQ.P-7wqs5aymtB5T1LgVj7c6I1OLNokKQzlDJ3HxeMV7f9FBs3vzkk2bIOHRqKg7WjfHP-0Gz8CgGEaWpCstwmYONGr3igafLaiMDI_KCOm2TNoHIXTEhVrT8bVU-0M47lFuIvVWO6DY5uGIn-n991iQ0OSMTgw6o8VmIw8AOH-1hAW7RxSTuKvg2GJPZyhmatzQRYZcchAqVgYeraCYCNTGUN7tHVxDy3FqMhEd0PWbjeJ2RTuWwletyCWqRiqRuCqlt93lG7BXF7NRL9yQIk-P5IcagEToWfsvFYG8_EVmxZxrVRK13kzbkH17yeuyPOtEi3RgyqOn77chLdfOOMxg",
          "expires_in": 39650,
          "scope": "admin read write",
          "jti": "3959c267-28ca-44a9-8a1e-7ace911a9f24"
        }
         */
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(UsernamePasswordAuthenticationToken.class);
    }
}

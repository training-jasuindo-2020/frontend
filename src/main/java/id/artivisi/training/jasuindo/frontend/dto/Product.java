package id.artivisi.training.jasuindo.frontend.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class Product {
    private String id;

    @NotNull @NotEmpty
    private String code;

    @NotNull @NotEmpty
    private String name;

    @NotNull
    private Organization organization;
}

package id.artivisi.training.jasuindo.frontend.controller;

import id.artivisi.training.jasuindo.frontend.dto.Organization;
import id.artivisi.training.jasuindo.frontend.service.BackendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller @Slf4j
public class OrganizationController {

    @Autowired private BackendService backendService;

    @GetMapping("/organization/form")
    public ModelMap tampilkanForm(@RequestParam(required = false)String id) {
        ModelMap mm = new ModelMap();

        if (id != null) {
            Organization org = backendService.findById(id);
            mm.addAttribute("organization", org);
        } else {
            mm.addAttribute("newdata", true);
            mm.addAttribute("organization", new Organization());
        }

        return mm;
    }

    @PostMapping("/organization/form")
    public String prosesForm(
            @ModelAttribute @Valid Organization org, BindingResult errors) {

        if (errors.hasErrors()) {
            log.info(errors.getAllErrors().toString());
            return "organization/form";
        }

        log.info(org.toString());

        if(org.getId() == null) {
            backendService.simpan(org);
        } else {
            backendService.update(org);
        }

        return "redirect:list";
    }

    @GetMapping("/organization/list")
    public ModelMap daftarOrganization() {
        return new ModelMap()
                .addAttribute("dataOrganisasi",
                        backendService.semuaOrganisasi());
    }
}

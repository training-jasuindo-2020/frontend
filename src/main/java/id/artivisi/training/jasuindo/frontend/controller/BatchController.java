package id.artivisi.training.jasuindo.frontend.controller;

import id.artivisi.training.jasuindo.frontend.dto.Batch;
import id.artivisi.training.jasuindo.frontend.dto.Organization;
import id.artivisi.training.jasuindo.frontend.dto.Product;
import id.artivisi.training.jasuindo.frontend.service.BackendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;

@Controller @Slf4j
@RequestMapping("/batch")
public class BatchController {

    @Autowired private BackendService backendService;

    @GetMapping("/form")
    public ModelMap displayForm() {
        Organization o = new Organization();
        Product p = new Product();
        Batch b = new Batch();

        b.setProduct(p);
        p.setOrganization(o);

        return new ModelMap()
                .addAttribute("batch", b);
    }

    @PostMapping("/form")
    public String processForm(@ModelAttribute @Valid Batch batch, BindingResult errors, SessionStatus status) {
        log.debug("Save Batch : {}", batch.toString());

        if (errors.hasErrors()) {
            return "batch/form";
        }
        backendService.simpan(batch);
        status.setComplete();
        return "redirect:form";
    }
}

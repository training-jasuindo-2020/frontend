package id.artivisi.training.jasuindo.frontend.service;

import id.artivisi.training.jasuindo.frontend.dto.Batch;
import id.artivisi.training.jasuindo.frontend.dto.Organization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service @Slf4j
public class BackendService {

    @Value("${backend.url}") private String backendUrl;
    @Autowired private WebClient webClient;

    public Organization[] semuaOrganisasi() {
        return webClient.get()
                .uri(backendUrl + "/api/organization/")
                .retrieve()
                .bodyToMono(Organization[].class)
                .block();
    }

    public Organization findById(String id) {
        return webClient.get()
                .uri(backendUrl + "/api/organization/"+id)
                .retrieve()
                .bodyToMono(Organization.class)
                .block();
    }

    public void simpan(Organization organization) {
        log.info("Save organization : {}", organization.toString());
        webClient.post()
                .uri(backendUrl + "/api/organization/")
                .bodyValue(organization)
                .exchange()
                .block();
    }

    public void update(Organization organization) {
        log.info("Update organization : {}", organization.toString());
        webClient.put()
                .uri(backendUrl + "/api/organization/"+organization.getId())
                .bodyValue(organization)
                .exchange()
                .block();
    }

    public void simpan(Batch batch) {
        webClient.post()
                .uri(backendUrl + "/api/batch/")
                .bodyValue(batch)
                .exchange()
                .block();
    }
}

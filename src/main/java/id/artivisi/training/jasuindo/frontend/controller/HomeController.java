package id.artivisi.training.jasuindo.frontend.controller;

import id.artivisi.training.jasuindo.frontend.dto.Organization;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDateTime;

@Controller @Slf4j
public class HomeController {

    @Autowired
    private WebClient webClient;
    @Autowired private OAuth2AuthorizedClientManager authorizedClientManager;


    @GetMapping("/home")
    public ModelMap homepage(@RequestParam(required = false) String nama) {
        ModelMap mm = new ModelMap();
        mm.addAttribute("waktu", LocalDateTime.now());
        mm.addAttribute("nama", nama);
        return mm;
    }

    @GetMapping("/accesstoken") @ResponseBody
    public OAuth2AccessToken accessToken(Authentication authentication) {

        log.debug("Current user : {}", authentication.getName());
        log.debug("Current user password : {}", authentication.getCredentials().toString());

        OAuth2AuthorizeRequest authorizeRequest = OAuth2AuthorizeRequest
                .withClientRegistrationId("backendapp")
                .principal(authentication)
                .build();
        OAuth2AuthorizedClient authorizedClient = this.authorizedClientManager.authorize(authorizeRequest);

        OAuth2AccessToken accessToken = authorizedClient.getAccessToken();
        return accessToken;
    }

    @GetMapping("/coba") @ResponseBody
    public Organization[] coba() {
        return webClient
                .get()
                .uri("http://backend:8080/api/organization/")
                .retrieve()
                .bodyToMono(Organization[].class)
                .block();
    }
}
